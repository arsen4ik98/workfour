import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static io.restassured.RestAssured.given;

public class Hamcrest {
    @ParameterizedTest
    @ValueSource(strings = {"Busya", "BUSYA", "busya", "BuSyA", "Буся", "БУСЯ", "буся", "БуСя", "busya"})
    void get(String names) {
        JSONObject body = new JSONObject()
                .put("id", 25)
                .put("name", names)
                .put("status", "available");

        given()
                .when()
                .body(body.toString())
                .contentType(ContentType.JSON)
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200);

        given()
                .when()
                .body(body.toString())
                .pathParam("id", 25)
                .get("https://petstore.swagger.io/v2/pet/{id}")
                .then()
                .statusCode(200)
                .assertThat()
                .body("name", Matchers.equalTo(names));
    }
}