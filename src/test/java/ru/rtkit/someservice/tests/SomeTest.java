package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import ru.rtkit.someservice.apiHelper.Endpoints;

@DisplayName("Вывод в Allure (5 задание)")
public class SomeTest extends BaseTest {

    @ParameterizedTest(name = "{displayName} [{argumentsWithNames}]")
    @ValueSource(strings = {"Busya", "BUSYA", "busya", "BuSyA", "Буся", "БУСЯ", "буся", "БуСя", "busya"})
    @DisplayName("Параметризация из задания 3")
    @Description("Тестирование метода PetStore")
    void get(String names) {
        JSONObject body = new JSONObject()
                .put("id", 25)
                .put("name", names)
                .put("status", "available");

        String id = api.post(Endpoints.NEW_PET, body.toString(), resp200)
                .jsonPath()
                .getString("id");

        api.get(Endpoints.PET_ID, resp200, id)
                .then()
                .assertThat()
                .body("name", Matchers.equalTo(names));
    }

    @Test
    @DisplayName("Проверка c методом @STEP")
    @Description("PetStore @STEP")
    void findPetByID() {
        String originalName = "Федя";
        String id = createNewPet(originalName);

        String name = api.get(Endpoints.PET_ID, resp200, id)
                .jsonPath()
                .getString("name");
        Assertions.assertEquals(originalName, name);
    }
    @Step("Создание питомца")
    String createNewPet(String name) {
        JSONObject body = new JSONObject()
                .put("id", 25)
                .put("name", name)
                .put("status", "available");

        return api.post(Endpoints.NEW_PET, body.toString(), resp200)
                .jsonPath()
                .getString("id");
    }


}
