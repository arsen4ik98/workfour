package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@DisplayName("SQLite 6 задание")
public class SQLite {

    String pathToDB = getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();

    private final String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Test
    @DisplayName("1 задание")
    @Description("Вывод таблицы albums")
    void get() {


        String query = "SELECT * FROM ALBUMS WHERE ALBUMID = 1";
        List<String> albums = new ArrayList<>();

        try {

            Connection conn = DriverManager.getConnection(dbUrl);
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                albums.add(rs.getString("title"));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        System.out.println("1 задание: " + albums);

    }
    @Test
    @DisplayName("2 задание")
    @Description("Вывод все записи с таблицы tracks, где в качестве жанра выбран \"Pop\".")
    void get2() {
        String query = "Select * from tracks where  Genreid in (Select GenreId  from genres where Name = 'Pop');";
        List<String> genres = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                genres.add(rs.getString("TrackId"));
                genres.add(rs.getString("name"));
                genres.add(rs.getString("AlbumId"));
                genres.add(rs.getString("MediaTypeId"));
                genres.add(rs.getString("GenreId"));
                genres.add(rs.getString("Composer"));
                genres.add(rs.getString("Milliseconds"));
                genres.add(rs.getString("Bytes"));
                genres.add(rs.getString("UnitPrice"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("2 задание: " + genres);
    }

}
