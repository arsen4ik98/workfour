package ru.rtkit.someservice.apiHelper;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class PetStoreApi {

    private final String URI = "https://petstore.swagger.io/v2";

    public PetStoreApi() {
        RestAssured.baseURI = URI;
        RestAssured.defaultParser = Parser.JSON;
        //       RestAssured.filters(new AllureRestAssured());
    }

    public Response get(String endpoint, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    public Response get(String endpoint, ResponseSpecification resp, Object pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint,pathParams);
    }


    public Response get(String endpoint, ResponseSpecification resp,
                        Map<String, String> queryParams) {
        return given()
                .params(queryParams)
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    public Response getWithAuth(String endpoint, ResponseSpecification
            resp) {
        return given()
                .header("api_key", "special-key")
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }
    //POST without path parameters

    public Response post(String endpoint, Object body,
                         ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }
    //POST with path parameters

    public Response delete(String endpoint, ResponseSpecification resp,
                           Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .delete(endpoint, pathParams);
    }
    //PUT with path parameters

    public Response put(String endpoint, Object body, ResponseSpecification
            resp, Object... pathParams) {
        return given()

                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .put(endpoint, pathParams);


    }
}
